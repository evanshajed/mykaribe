<?php
/**
 * Job Listing: Products
 *
 * @since Listify 1.0.0
 */
class Listify_Widget_Listing_Products_TANWID extends Listify_Widget {

	public function __construct() {
		$this->widget_description = __( 'Display the listings products with Ticket', 'listify' );
		$this->widget_id          = 'listify_widget_panel_listing_products_TanWid';
		$this->widget_name        = __( 'Listify - Listing: Products Tickets', 'listify' );
		$this->widget_areas       = array( 'single-job_listing' );
		$this->widget_notice      = __( 'Add this widget only in "Single Listing - Sidebar" widget area.' );
		$this->settings           = array(
			'title' => array(
				'type'  => 'text',
				'std'   => 'TICKET BOOKING',
				'label' => __( 'Title:', 'listify' ),
			),
			'icon' => array(
				'type'    => 'text',
				'std'     => '',
				'label'   => '<a href="http://ionicons.com/">' . __( 'Icon Class:', 'listify' ) . '</a>',
			),
		);

		parent::__construct();

        if ( is_active_widget( false, false, 'listify_widget_panel_listing_products_tanwid', true ) ) {
            add_action( 'listify_single_job_listing_actions_after', array( $this, 'output_button' ) );
        }

	}


    public function output_button() {
        global $post, $listify_woocommerce_bookings;

        $products = $this->get_Ticket_products( $post->ID );

        if ( ! $products ) {
            return;
        }

        echo '<a style="float: right; margin-left: 15px; " href="#listify_widget_panel_listing_products_tanwid-2" class="button">' . __( 'Buy a Ticket', 'listify' ) . '</a>';
    }


    public function get_Ticket_products( $post_id ) {
        $products = wpjmp_get_products( $post_id );

        if ( ! $products || empty( $products ) ) {
            return;
        }

        $_products = array();

        foreach ( $products as $product ) {
            $product = wc_get_product( $product );

            if ( ! $product || ! wc_box_office_is_product_ticket( $product ) ) {
                continue;
            }

            $_products[] = $product;
        }

        if ( empty( $_products ) ) {
            return false;
        }

        return $_products;
    }





	function widget( $args, $instance ) {
		global $job_preview, $job_manager, $post;

		if ( ! is_singular( 'job_listing' ) && ! $job_preview ) {
			echo $this->widget_areas_notice(); // WPCS: XSS ok.
			return false;
		}

		if ( 'preview' == $post->post_status ) {
			return;
		}

		extract( $args );

		$products = get_post_meta( $post->ID, '_products', true );

		// Stop if there are no products
		if ( ! $products || ! is_array( $products ) ) {
			return;
		}

		$args = apply_filters( 'woocommerce_related_products_args', array(
			'post_type'            => 'product',
			'ignore_sticky_posts'  => 1,
			'no_found_rows'        => 1,
			'posts_per_page'       => -1,
			'post__in'             => $products,
		) );

		$products = new WP_Query( $args );

		if ( ! $products->have_posts() ) {
			return;
		}


        ///////// empty widget condition start
        $cd=0;
        while ( $products->have_posts() ) {
            $products->the_post();
            $_product = wc_get_product($products->post->ID);
            $tanv = $_product->get_type();

            $is_ticket = get_post_meta(get_the_ID(), '_ticket', true );

            //echo $tanv;
            //echo get_post_meta(get_the_ID(), '_ticket', true );

            if($is_ticket=="yes"){
                $cd=$cd+1;
                //echo $cd;
            }
        }
        if($cd==0){
            //echo "here: ".$count;
            return;
        }
        ///////// empty widget condition end



		$title = apply_filters( 'widget_title', isset( $instance['title'] ) ? $instance['title'] : '', $instance, $this->id_base );
		$icon = isset( $instance['icon'] ) ? $instance['icon'] : null;

		if ( $icon ) {
			if ( strpos( $icon, 'ion-' ) !== false ) {
				$before_title = sprintf( $before_title, $icon );
			} else {
				$before_title = sprintf( $before_title, 'ion-' . $icon );
			}
		}

		ob_start();

		echo $before_widget;

		if ( $title ) {
			echo $before_title . $title . $after_title;
		}

		echo '<div class="woocommerce">';
		echo '<ul class="product_list_widget">';

		while ( $products->have_posts() ) {
			$products->the_post();

            global $product;

            ?>

            <li>
                <?php
                    if ( $product->is_in_stock() ) :
                        if ( wc_box_office_is_product_ticket( $product ) ) {
                            ?>

                            <a href="<?php echo esc_url( $product->get_permalink() ); ?>">
                                <?php echo $product->get_image(); ?>
                                <span class="product-title"><?php echo $product->get_name(); ?></span>
                            </a>
                            <?php if ( ! empty( $show_rating ) ) : ?>
                                <?php echo wc_get_rating_html( $product->get_average_rating() ); ?>
                            <?php endif; ?>

                            <?php echo $product->get_price_html(); ?>

                    <?php do_action( 'woocommerce_before_add_to_cart_form' ); ?>

                    <form class="cart" method="post" enctype='multipart/form-data'>

                        <?php
                            do_action( 'woocommerce_before_add_to_cart_button' );
                            do_action( 'woocommerce_before_add_to_cart_quantity' );
                            woocommerce_quantity_input( array(
                                'min_value'   => apply_filters( 'woocommerce_quantity_input_min', $product->get_min_purchase_quantity(), $product ),
                                'max_value'   => apply_filters( 'woocommerce_quantity_input_max', $product->get_max_purchase_quantity(), $product ),
                                'input_value' => isset( $_POST['quantity'] ) ? wc_stock_amount( $_POST['quantity'] ) : $product->get_min_purchase_quantity(),
                            ) );
                            do_action( 'woocommerce_after_add_to_cart_quantity' );
                        ?>
                        <button type="submit" name="add-to-cart" value="<?php echo esc_attr( $product->get_id() ); ?>" class="single_add_to_cart_button button alt"><?php echo esc_html( $product->single_add_to_cart_text() ); ?></button>
                        <?php //echo $this->id_base ?>
                        <?php   do_action( 'woocommerce_after_add_to_cart_button' );     ?>

                    </form>
                            <?php
                            do_action( 'woocommerce_after_add_to_cart_form' );



                        }
                    endif;
                ?>

            </li>

            <?php

		}

		echo '</ul>';
		echo '</div>';

		echo $after_widget;

		$content = ob_get_clean();

		echo apply_filters( $this->widget_id, $content );

		wp_reset_query();
	}
}