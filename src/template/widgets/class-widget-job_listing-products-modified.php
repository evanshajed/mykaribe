<?php
/**
 * Job Listing: Products
 *
 * @since Listify 1.0.0
 */
class Listify_Widget_Listing_Products_Modified extends Listify_Widget {

	public function __construct() {
		$this->widget_description = __( 'Display the listings products Modified (sidebar)', 'listify' );
		$this->widget_id          = 'listify_widget_panel_listing_products_modified';
		$this->widget_name        = __( 'Listify - Listing: Products MODIFIED (Sidebar)', 'listify' );
		$this->widget_areas       = array( 'single-job_listing' );
		$this->widget_notice      = __( 'Add this widget only in "Single Listing - Sidebar" widget area.' );
		$this->settings           = array(
			'title' => array(
				'type'  => 'text',
				'std'   => 'Related Products',
				'label' => __( 'Title:', 'listify' ),
			),
			'icon' => array(
				'type'    => 'text',
				'std'     => '',
				'label'   => '<a href="http://ionicons.com/">' . __( 'Icon Class:', 'listify' ) . '</a>',
			),
		);

		parent::__construct();
	}

	function widget( $args, $instance ) {
		global $job_preview, $job_manager, $post;

		if ( ! is_singular( 'job_listing' ) && ! $job_preview ) {
			echo $this->widget_areas_notice(); // WPCS: XSS ok.
			return false;
		}

		if ( 'preview' == $post->post_status ) {
			return;
		}

		extract( $args );

		$products = get_post_meta( $post->ID, '_products', true );


        /*
        if(is_array($products)){
            foreach($products as $p=>$l){
                echo $p.'---'.$l.'<br>';
                if(!empty($l)){
                    $_product = wc_get_product( $l );
                    echo $_product->get_type();
                    echo "<br>";
                    if( $_product->is_type( 'booking' ) || $_product->is_type( 'accommodation-booking' ) ) {
                        echo "booking product or accommodation-booking product"."<br>";
                    } else {
                        echo "not those 2"."<br>";
                    }
                }
                echo "<br><br>";
            }
        }
        */


		// Stop if there are no products
		if ( ! $products || ! is_array( $products ) ) {
			return;
		}

		$args = apply_filters( 'woocommerce_related_products_args', array(
			'post_type'            => 'product',
			'ignore_sticky_posts'  => 1,
			'no_found_rows'        => 1,
			'posts_per_page'       => -1,
			'post__in'             => $products,
		) );

		$products = new WP_Query( $args );

		if ( ! $products->have_posts() ) {
			return;
		}

        ///////// empty widget condition start
        $count=0;
        $other=0;
        $empty=1;
        while ( $products->have_posts() ) {
            $products->the_post();

            $_product = wc_get_product($products->post->ID);

            $tanv = $_product->get_type();

            //echo $tanv;
            $is_ticket = get_post_meta(get_the_ID(), '_ticket', true );

            if($tanv=="booking" || $tanv=="accommodation-booking" || $is_ticket=="yes"){
                $count=$count+1;
            }
            if($tanv!=="booking" && $tanv!=="accommodation-booking" && $is_ticket!=="yes"){
                $other=$other+1;
            }
        }
//echo "Count-" . $count . "<br>";
//echo "Other-" . $other . "<br>";
        if($count>0 && $other==0){
//echo "Here: ".$count;
            $empty=0;
            //return;
        }
        ///////// empty widget condition end

		$title = apply_filters( 'widget_title', isset( $instance['title'] ) ? $instance['title'] : '', $instance, $this->id_base );
		$icon = isset( $instance['icon'] ) ? $instance['icon'] : null;

		if ( $icon ) {
			if ( strpos( $icon, 'ion-' ) !== false ) {
				$before_title = sprintf( $before_title, $icon );
			} else {
				$before_title = sprintf( $before_title, 'ion-' . $icon );
			}
		}
////////////////

        ob_start();

        if($empty==0){

        }else{
            echo $before_widget;
        }
//		echo $before_widget;

		if ( $title ) {
            if($empty==0){

            }else{
                echo $before_title . $title . $after_title;
            }
			//echo $before_title . $title . $after_title;
		}

		echo '<div class="woocommerce">';
		echo '<ul class="product_list_widget">';

        //$products_tan = get_post_meta( $post->ID, '_products', true );

		while ( $products->have_posts() ) {

/*
if (is_array($products_tan)) {
    foreach ($products_tan as $p => $l) {
        //echo $p . '---' . $l . '<br>';
        if (!empty($l)) {
            $_product = wc_get_product($l);
            //echo $_product->get_type();
            echo "<br>";
            if ($_product->is_type('booking') || $_product->is_type('accommodation-booking')) {
                echo "booking product or accommodation-booking product" . "<br>";
                continue;
            } else
            {
                echo "not those 2" . "<br>";
                $products->the_post();
                wc_get_template( 'content-widget-product.php' );
            }
        }
        //echo "<br><br>";
    }
}
            */

			$products->the_post();
			//wc_get_template( 'content-widget-product.php' );


            $_product = wc_get_product($products->post->ID);
            $tanv = $_product->get_type();
            //booking accommodation-booking
            $is_ticket = get_post_meta(get_the_ID(), '_ticket', true );

            if($tanv=="booking" || $tanv=="accommodation-booking" || $is_ticket=="yes")
            {

            }
            else
            {
                wc_get_template( 'content-widget-product.php' );
            }
		}

		echo '</ul>';
		echo '</div>';

		echo $after_widget;

		$content = ob_get_clean();

		echo apply_filters( $this->widget_id, $content );

		wp_reset_query();
////////////////////////////


	}

    /*
    public function product_data_fetch_tanv($products_tan)
    {
        if (is_array($products_tan)) {
            foreach ($products_tan as $p => $l) {
                echo $p . '---' . $l . '<br>';
                if (!empty($l)) {
                    $_product = wc_get_product($l);

                    echo $_product->get_type();
                    echo "<br>";

                    if ($_product->is_type('booking') || $_product->is_type('accommodation-booking')) {

                        echo "booking product or accommodation-booking product" . "<br>";

                    } else {

                        echo "not those 2" . "<br>";
                    }
                }
                echo "<br><br>";
            }
        }
    }
    */
}