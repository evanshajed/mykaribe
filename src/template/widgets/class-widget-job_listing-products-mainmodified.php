<?php
/**
 * Job Listing: Products
 *
 * @since Listify 1.0.0
 */
class Listify_Widget_Listing_Products_Mainmodified extends Listify_Widget {

	public function __construct() {
		$this->widget_description = __( 'Display the listings products Modified (main content)', 'listify' );
		$this->widget_id          = 'listify_widget_panel_listing_products_main_modified';
		$this->widget_name        = __( 'Listify - Listing: Products MODIFIED (Content)', 'listify' );
		$this->widget_areas       = array( 'single-job_listing-widget-area' );
		$this->widget_notice      = __( 'Add this widget only in "Single Listing - Main Content" widget area.' );
		$this->settings           = array(
			array(
				'type' => 'description',
				'std' => __( 'This widget has no options', 'listify' ),
			),
		);

		$wpjmp = WPJMP();

		remove_action( 'single_job_listing_end', array( $wpjmp->products, 'listing_display_products' ) );

		parent::__construct();
	}

	function widget( $args, $instance ) {
		global $job_preview, $job_manager, $post;

		if ( ! is_singular( 'job_listing' ) && ! $job_preview ) {
			echo $this->widget_areas_notice(); // WPCS: XSS ok.
			return false;
		}

		if ( 'preview' == $post->post_status ) {
			return;
		}

		extract( $args );

		$wpjmp = WPJMP();

		ob_start();

//$wpjmp->products->listing_display_products();




        $products = get_post_meta( $post->ID, '_products', true );

        /*
        foreach($products as $t=>$a){
            echo $t."_-_".$a;
            echo "<br>";
        }
        */

        /*_____________________________________*/

        if ( ! $products || ! is_array( $products ) ) {
            return;
        }




        ////////////////////////////////////////////





        $args = apply_filters( 'woocommerce_related_products_args', array(
            'post_type'            => 'product',
            'ignore_sticky_posts'  => 1,
            'no_found_rows'        => 1,
            'posts_per_page'       => -1,
            'post__in'             => $products,
        ) );

        $products = new WP_Query( $args );

        if ( $products->have_posts() ) : ?>

            <div class="listing products woocommerce">


                <!--    start    -->
                <?php woocommerce_product_loop_start();

                $co = 0;
                while ( $products->have_posts() ){
                    $products->the_post();

                    $_producT = wc_get_product($products->post->ID);
                    $tanv1 = $_producT->get_type();
                    //booking accommodation-booking
                    $is_ticket = get_post_meta(get_the_ID(), '_ticket', true );

                    if($tanv1=="booking" || $tanv1=="accommodation-booking" || $is_ticket=="yes"){

                    }else{
                        $co=$co+1;
                    }
                }

                woocommerce_product_loop_end();
                ?>
                <!--    end    -->

                <h2>
                    <?php
                        //echo $co;
                        if($co>0){
                            //echo "co is :".$co."<br>";
                            echo get_option( 'wpjmp_listing_products_text' );
                        }else{

                        }

                    ?>
                </h2>


                <?php woocommerce_product_loop_start();


                while ( $products->have_posts() ){
                    $products->the_post();

                    $_product = wc_get_product($products->post->ID);
                    $tanv = $_product->get_type();
                    //booking accommodation-booking
                    $is_ticket = get_post_meta(get_the_ID(), '_ticket', true );

                    if($tanv=="booking" || $tanv=="accommodation-booking" || $is_ticket=="yes")
                    {

                    }else{
                        wc_get_template_part( 'content', 'product' );
                    }


                }

                woocommerce_product_loop_end(); ?>

            </div>

        <?php endif;
        wp_reset_postdata();

        /*____________________________________*/

		echo apply_filters( $this->widget_id, ob_get_clean() );
	}
}
