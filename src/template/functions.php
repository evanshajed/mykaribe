<?php
/**
 * Listify child theme.
 */

// Function to remove WCFM plugin css
function custom_dequeue() {
    // Deregistering Core

        wp_dequeue_style('wcfm_core_css');
        wp_deregister_style('wcfm_core_css');

        // Deregistering Admin bar

        wp_dequeue_style('wcfm_admin_bar_css');
        wp_deregister_style('wcfm_admin_bar_css');

}

add_action( 'wp_enqueue_scripts', 'custom_dequeue', 9999 );
add_action( 'wp_header', 'custom_dequeue', 9999 );

/**
 * Plugin Name: Listify - Limit Photo Uploads to Listing Owner
 */
function limit_who_can_upload($can) {
    global $post;
    if ( $post->post_author == get_current_user_id() ) {
        return $can;
    }
    return false;
}
add_filter( 'listify_can_upload_to_listing','limit_who_can_upload' );



// Function to load Dashboard Fonts
function dashboard_fonts(){
  if(is_page_template( $template = 'wcfm/template-wcfm-dashboard.php' )){
    wp_enqueue_script('listify-child', 'https://cdnjs.cloudflare.com/ajax/libs/webfont/1.6.28/webfontloader.js');
    wp_add_inline_script( 'listify-child', 'WebFont.load({
      google: {
        families: ["Lato", "Open Sans"]
      }
    });');
  }
}

// Function for listify child dashboard style which only loads when WCFM Dashboard pages are active

function listify_child_styles() {
  if(is_page_template( $template = 'wcfm/template-wcfm-dashboard.php' )){
    wp_enqueue_style( 'listify-child', get_stylesheet_uri(), array(listify),'1.9.9' );
  }

}


add_action( 'wp_enqueue_scripts', 'listify_child_styles', 999 );

add_action('wp_enqueue_scripts','dashboard_fonts');


/**
 * WCFM redirect bookings menu page to booking list. Sean de Freitas
 */

function wcfm_custom_booking_menu( $wcfm_menus ) {
  if( isset( $wcfm_menus['wcfm-bookings-dashboard'] ) ) {
    $wcfm_menus['wcfm-bookings-dashboard']['url'] = get_wcfm_bookings_url();
  }
  return $wcfm_menus;
}

add_filter( 'wcfm_menus', 'wcfm_custom_booking_menu', 50 );

/**
* Return the permalink of the shop page for the continue shopping redirect filter
*
* @param string $return_to
* @return string
*/
function my_woocommerce_continue_shopping_redirect( $return_to ) {
return get_permalink( wc_get_page_id( 'shop' ) );
}
add_filter( 'woocommerce_continue_shopping_redirect', 'my_woocommerce_continue_shopping_redirect', 20 );


// Custom Ticket Widget by - Tanvir
function custom_widgets_init() {
    include_once( dirname( __FILE__ ) . '/widgets/class-widget-job_listing-products_TANWID.php' );
    register_widget( 'Listify_Widget_Listing_Products_TANWID' );

}
add_action( 'widgets_init', 'custom_widgets_init', 20 );


function custom_widgets_init2() {

    include_once( dirname( __FILE__ ) . '/widgets/class-widget-job_listing-products-mainmodified.php' );
    register_widget( 'Listify_Widget_Listing_Products_Mainmodified' );
}
add_action( 'widgets_init', 'custom_widgets_init2', 20 );

function custom_widgets_init3() {

    include_once( dirname( __FILE__ ) . '/widgets/class-widget-job_listing-products-modified.php' );
    register_widget( 'Listify_Widget_Listing_Products_Modified' );
}
add_action( 'widgets_init', 'custom_widgets_init3', 20 );




/*
function custom_listify_single_job_listing_actions_after() {

    if(is_active_widget( false,false, 'listify_widget_panel_listing_products_tanwid',true))
    {
        echo '<a style="float: right; margin-left: 15px; " href="#listify_widget_panel_listing_products_tanwid-2" class="button">Buy a Ticket</a>';
    }

}
*/



//add_filter( 'listify_single_job_listing_actions_after', 'custom_listify_single_job_listing_actions_after' );