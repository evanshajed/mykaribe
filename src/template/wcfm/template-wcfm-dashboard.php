<?php
/**
 * Template Name: Page: WCFM Dashboard
 * Description: A custom page template for WCFM Dashboard View
 *
 */
 get_header(); ?>

    <!-- Custom page Container for WCFM-Dashboard -->
    <div id="primary" class="container-fluid">

      <div class="row">
        <div class="col-md-12">
          <main id="main" class="site-main wc-db-main" role="main">
            <?php echo do_shortcode("[wc_frontend_manager]"); ?>
          </main>

        </div>
      </div>

    </div>

    <!-- #Custom page Container for WCFM-Dashboard -->

 <?php get_footer(); ?>